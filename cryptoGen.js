const NodeRSA = require('node-rsa');
const KJUR = require('jsrsasign')

let cryptoGen = {}
cryptoGen.generateKeyPair = (bits={b:2048},exponent={e:65537})=>{
    let key = new NodeRSA(bits,exponent);
    key.generateKeyPair();
    let privateKey = key.exportKey('pkcs8-private-pem');
    let publicKey =  key.exportKey('pkcs8-public-pem');

    return {privateKey:privateKey,publicKey:publicKey};
};

cryptoGen.sign = (privateKey,payload)=>{
    var sig = new KJUR.crypto.Signature({'alg':'SHA256withRSA'});
    sig.init(privateKey);
    sig.updateString(payload);
    var sigValueHex = sig.sign();
    return sigValueHex;
}
cryptoGen.verify= (publicKey,signature,payload)=>{
    var sig = new KJUR.crypto.Signature({'alg':'SHA256withRSA'});
    sig.init(publicKey);
    sig.updateString(payload);
    return sig.verify(signature);
}

module.exports=cryptoGen