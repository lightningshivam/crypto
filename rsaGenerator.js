const NodeRSA = require('node-rsa');
const crypto = require('crypto');

let rsaGenerator = {};

rsaGenerator.generateKeyPair = ()=>{
    let key = new NodeRSA();
    key.generateKeyPair();
    let privateKey = key.exportKey('pkcs8-private-pem');
    let publicKey =  key.exportKey('pkcs8-public-pem');

    return {privateKey:privateKey,publicKey:publicKey};
};

rsaGenerator.sign = (privateKey,payload)=> {
    const sign = crypto.createSign('SHA256');
    sign.update(payload);
    sign.end();
    const signature = sign.sign(privateKey);
    return signature;
}

rsaGenerator.verify = (publicKey,signature,payload)=>{
    const verify = crypto.createVerify('SHA256');
    verify.update(payload);
    verify.end();
    let result = verify.verify(publicKey, signature);
    return result;
}
module.exports = rsaGenerator;