const NodeRSA = require('node-rsa');
const crypto = require('crypto');
const KJUR = require('jsrsasign')


// let sig = key.sign(Buffer.from(privateKey));
// let verify = key.verify(Buffer.from(privateKey),sig);
// console.log(sig);
// console.log(verify);



// const { privateKey, publicKey } = crypto.generateKeyPairSync('rsa', {
//   modulusLength: 2048,
// });
let nodersa = {};
let key = new NodeRSA();
key.generateKeyPair();
let privateKey = key.exportKey('pkcs8-private-pem');
let publicKey =  key.exportKey('pkcs8-public-pem');

const sign = crypto.createSign('SHA256');
sign.update('some data to sign');
sign.end();
const signature = sign.sign(privateKey);


const verify = crypto.createVerify('SHA256');
verify.update('some data to sign');
verify.end();
console.log(verify.verify(publicKey, signature));


nodersa.generateKeyPair = ()=>{
    let key = new NodeRSA();
    key.generateKeyPair();
    let privateKey = key.exportKey('pkcs8-private-pem');
    let publicKey =  key.exportKey('pkcs8-public-pem');

    return {privateKey:privateKey,publicKey:publicKey};
};

nodersa.sign = (privateKey,payload)=> {
    const sign = crypto.createSign('SHA256');
    sign.update(payload);
    sign.end();
    const signature = sign.sign(privateKey);
    return signature;
}

nodersa.verify = (publicKey,signature,payload)=>{
    const verify = crypto.createVerify('SHA256');
    verify.update(payload);
    verify.end();
    let result = verify.verify(publicKey, signature);
    return result;
}
nodersa.signJs = (publicKey,signature,payload)=>{
   var sig = new KJUR.crypto.Signature({'alg':'SHA1withRSA'});
}
module.exports = nodersa;