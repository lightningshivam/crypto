'use strict';

document.write("<h1>Hello World!!!</h1>");
// var btnHello = document.createElement("BUTTON");
// btnHello.onclick = hello();

// var t = document.createTextNode("CLICK ME");       // Create a text node
// btnHello.appendChild(t);                                // Append the text to <button>
// document.body.appendChild(btnHello);                    // Append <button> to <body>


function setCookie(cname,cvalue,exdays){
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*100));
    var expires = "expires="+d.toUTCString;
    document.cookie = cname+"="+cvalue+";"+expires+";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == '') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
      alert("Welcome again " + user);
    } else {
      user = prompt("Please enter your name:", "");
      if (user != "" && user != null) {
        setCookie("username", user, 365);
      }
    }
  }
// checkCookie()

const openpgp = require('openpgp');

function genrateKeys(){
    var passphrase='super long and hard to guess secret';
    var options = {
        userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
        numBits: 2048,                                            // RSA key size
        passphrase: passphrase       // protects the private key
    };

    openpgp.generateKey(options).then(function(key) {
        var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
        var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        
        encryptDataSaveKey({privateKey:privkey,publicKey:pubkey,revokKey:revocationCertificate,passphrase:passphrase});
        console.log('data stored');

    });     
}
// genrateKeys()
function callOnStore(fn_) {

	// This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
	var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
 
	// Open (or create) the database
	var open = indexedDB.open("MyDatabase", 1);

	// Create the schema
	open.onupgradeneeded = function() {
	    var db = open.result;
	    var store = db.createObjectStore("MyObjectStore", {keyPath: "id"});
	};


	open.onsuccess = function() {
	    // Start a new transaction
	    var db = open.result;
	    var tx = db.transaction("MyObjectStore", "readwrite");
	    var store = tx.objectStore("MyObjectStore");

	    fn_(store)


	    // Close the db when the transaction is done
	    tx.oncomplete = function() {
	        db.close();
	    };
	}
}

async function encryptDataSaveKey(keys) {
	callOnStore(function (store) {
		store.put({id: 1, keys: keys});
	})
}

function loadKeyDecryptData() {
	callOnStore(function (store) {
    var getData = store.get(1);
    getData.onsuccess = async function() {
    	var keys = getData.result.keys;
        console.log("key: ", keys.privateKey);
        (async () => {
            let options;
        
            const pubkey = keys.publicKey; // Public key
            const privkey = keys.privateKey; // Encrypted private key
            const passphrase = keys.passphrase; // Password that privKey is encrypted with
            const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0];
            // await privKeyObj.decrypt(passphrase);
        
 
           
            // console.log(ciphertext)

            var cleartext;

            options = {
                message: openpgp.message.fromText(readableStream), // CleartextMessage or Message object
                privateKeys: [privKeyObj]                             // for signing
            };
            await openpgp.sign(options).then(function(signed) {
                cleartext = signed.data; // '-----BEGIN PGP SIGNED MESSAGE ... END PGP SIGNATURE-----'
            });

            // const plaintext = await openpgp.stream.readToEnd(cleartext); // 'Hello, World!'
            // console.log(plaintext);
            var validity;
            options = {
                message: await openpgp.message.readArmored(cleartext), // parse armored message
                publicKeys: (await openpgp.key.readArmored(pubkey)).keys // for verification
            };
            
            openpgp.verify(options).then(function(verified) {
                validity = verified.signatures[0].valid; // true
                if (validity) {
                    console.log('signed by key id ' + verified.signatures[0].keyid.toHex());
                }else{
                    console.log('failed')
                }
            });

            // const readableStream = new ReadableStream({
            //     start(controller) {
            //         controller.enqueue('Hello, world!');
            //         controller.close();
            //     }
            // });
            // options = {
            //     message: openpgp.message.fromText(readableStream),        // input as Message object
            //     publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for encryption
            //     privateKeys: [privKeyObj]                                 // for signing (optional)
            // };
            // 
            // const encrypted = await openpgp.encrypt(options);
            // const ciphertext = encrypted.data; // ReadableStream containing '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
            // 
            // options = {
            //     message: await openpgp.message.readArmored(ciphertext),   // parse armored message
            //     publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for verification (optional)
            //     privateKeys: [privKeyObj]                                 // for decryption
            // };
            // 
            // const decrypted = await openpgp.decrypt(options);
            // console.log(decrypted);
            // const plaintext = await openpgp.stream.readToEnd(decrypted.data); // 'Hello, World!'
            // console.log(plaintext);
        })();

	   };
	})
}
  
loadKeyDecryptData()
